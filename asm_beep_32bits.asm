;;; Simple beep routines using PIT in 32-bit x86 assembly.
;;;
;;;
;;; Date added: 2023-11-17
;;;
;;; Copyright:  (C) 2023 Jiří Wolker <projects@jwo.cz>
;;;
;;;
;;;               --- Do whatever you want with this code! ---
;;;
;;;
;;; === Description ===
;;;
;;; These four routines can be used to add PC Speaker support to 32-bit (and
;;; 64-bit, but I did not test that) protected-mode programs support for the PC
;;; speaker. There routines are very eary to use, since:
;;;
;;;     (i)  The frequency is specified in Hertz
;;;
;;;    (ii)  No BIOS calls are made -- Compatible even with EFI systems!
;;;
;;;   (iii)  No interrupts are used -- You don't need to touch GDT, IDT etc.
;;;
;;;    (iv)  You can wait for some time *without using interrupts* (busy waiting
;;;          is used)
;;;
;;;     (v)  No other hardware except the channel 2 of the PIT (which is
;;;          designed to be used to drive the speaker) is used. Even the
;;;          beep_wait function does not use any other timer.
;;;
;;; Four routines are provided:
;;;
;;;     beep            Takes frequency in Hz from EDX and sets up the keyboard
;;;                     controller and the PIT to drive the PC Speaker at the
;;;                     specified frequency.
;;;
;;;     beep_stop       Stops the speaker. The timer will be kept running, so
;;;                     you can use beep_wait to produce silence. You can also
;;;                     run the function beep with zero frequency to stop the
;;;                     beep.
;;;
;;;     beep_resume     Resumes beep that was stopped by beep_stop.
;;;
;;;     beep_wait       Waits for the PIT ch. 2 timer to elapse EDX times. This
;;;                     means that if the last beep frequency was f Hz, then the
;;;                     delay will be long (f / EDX) seconds.
;;;
;;; If you do not want to compute EDX to pass to beep_wait to produce silent
;;; delays, set the frequency to 1000 Hz and immediatelly run beep_stop:
;;;
;;;     mov     edx, 1000
;;;     call    beep
;;;     call    beep_stop
;;;     mov     edx, 42         ; 42 *milliseconds*
;;;     call	beep_wait
;;;
;;; You can also easily produce beepcodes with length in milliseconda by setting
;;; the frequency to 1000 Hz and then running beep_stop and beep_resume
;;; interleaved with calls to beep_wait which takes the beep/silence length in
;;; milliseconds in this case.
;;;
;;; All these functions do not preserve register values and use only EAX and EDX
;;; registers. EFLAGS status after return is undefined.
;;;
;;; Enjoy!

example:
	;; Half-second beep at 2000 Hz:
	mov	edx, 2000
	call	beep
	mov	edx, 1000       ; 2000 Hz / 1000 ticks = 0.5 seconds
	call	beep_wait
	;; beep_wait zeroes edx.
	call	beep_stop

end:	jmp	end             ; End of the example program.



	;; Setup PC speaker and start beeping.
	;;
	;; In:  edx             frequency in Hz
	;; Overwrites eax, edx.
	;;
	;; Use frequency of 0 to disable the speaker.
beep:
	;; Allow stopping the speaker by setting requency to 0.
	;;
	;; (Comment-out these two lines to save a little bit of memory if you
	;;  use only beep_stop to stop the speaker.)
	or	edx, edx
	jz	beep_stop

	;; We'll configure PIT counter 2, in square gen mode.
	mov	al, 0xb6
	out	0x43, al
	;; Calculate the frequency (=> ax).
	mov	eax, 1193180    ; PIT base freq
	push	edx
	xor	edx, edx
	div	dword [esp]
	add	esp, 4
	;; Write the frequency to the PIT.
	out	0x42, al
	shr	ax, 8
	out	0x42, al

	;; FALLTHROUGH

	;; Re-enable PC speaker silenced using beep_stop.
	;;
	;; Overwrites ax.
beep_resume:
	;; Bind the speaker to the PIT.
	in	al, 0x6132      ; KB controller status
	or	al, 0x03
	out	0x61, al

	ret

	;; Disable the PC speaker.
	;;
	;; Overwrites al.
beep_stop:
	;; We'll keep the PIT time running to allow usage of beep_resume.
	;; Just unbind the speaker from the PIT.
	in	al, 0x6132      ; KC controller status
	and	al, ~0x03
	out	0x61, al

	ret

	;; Wait the specified number of PC speaker cycles.
	;;
	;; In:  edx             number of cycles
        ;; Out: edx             set to zero
	;;
	;; Overwrites eax.
beep_wait:
	or	edx, edx
	jnz	.wait
	ret

.wait:
	dec	edx

	mov	al, 0xe8        ; Setup PIT ch. 2 readback
	out	0x43, al
	in	al, 0x42        ; Read the state at function start
	mov	ah, al          ; Store it in AH

	;; Busy wait for the output pin change.
.loop:
	pause
	mov	al, 0xe8        ; Setup PIT ch. 2 readback
	out	0x43, al
	in	al, 0x42        ; Read current state
	xor	al, ah          ; Is the bit 7 different?
	and	al, 0x80        ;
	jnz	beep_wait       ; (if yes)
	jmp	.loop
